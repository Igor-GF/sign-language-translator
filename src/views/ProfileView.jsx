import React, { useContext, useEffect } from 'react';
import Profile from '../components/Profile/Profile';

//-- This is used to depict the profile
const ProfileView = () => {
  
  return (
    <>
      <div className="pt-4 pb-4">
        <Profile />
      </div>
    </>
  )
}

export default ProfileView;