import { apiURL } from "../../api"
import { apiKey } from "../../api"

//--- Function to check if logged in or not
export async function loginUser(credentials, setCredentials) {
    
    return fetch(`${apiURL}/translations?username=${credentials.username}`)
        .then(response => response.json())
        .then(results => {
            //-- If user does not exists -> Register
            if (results.length === 0) {
                registerUser(credentials)
            }
            else {
                // If user does exists -> Login
                
                //-- set the user correctly
                let user = results[0]
                if (user == null) {
                    user = results;
                }

                //-- credentials with new data
                credentials.id = user.id
                credentials.translations = user.translations
                setCredentials(credentials)
            }
        })
        .catch(error => {
        })
}

//-- Function to check if the user is registered
async function registerUser(credentials) {
    return fetch(`${apiURL}/translations`, {
        method: 'POST',
        headers: {
            'X-API-Key': apiKey,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(credentials)
    })
    .then(async (response) => {
        if (!response.ok) {
        const {error = 'An unknown error occured'} = await response.json()
        throw new Error(error)
        }
        // By this point, the user is registered and should login.
        loginUser(credentials)
        return response.json()
        
    })
}