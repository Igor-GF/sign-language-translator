import React, { useState, useEffect, useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import {LoginStateContext} from '../../Contexts/LoginStateContext';
import {UserContext} from '../../Contexts/UserContext';

const Profile = () => {

  const { credentials, setCredentials } = useContext(UserContext);
  const { loginState, setLoginState } = useContext(LoginStateContext);
  const [ tenTranslations, setTenTranslations ] = useState([]);

  const navigate = useNavigate();

  function goToTranslate() {
    navigate('/translator');
  }

  //-- Side Effects
  useEffect(() => {

      if (loginState === false) {
          navigate('/')
      }     
      
      setTenTranslations(credentials.translations.slice(0, 10));

      console.log(tenTranslations);

  }, [loginState] )

  return (
    <>
      <div class="mb-4 mt-1">
        <h2>Hello, { credentials.username }!</h2>
      </div>
      
      <div class="border border-primary rounded-3 p-4  mb-3">
        <h5 class="text-primary">Your most recent translations:</h5>   
        
        <ul className="list">
          {
            tenTranslations.map((item, index) => {
              return <li key={index}>{item}</li>
            })
          }
        </ul>
      </div>

      <div className="d-flex col-6">
        <div className="row">
          <button onClick={goToTranslate} className="btn btn-primary col ml-1" > Translate more </button>
        </div>
      </div> 
    </>
  )
}

export default Profile;