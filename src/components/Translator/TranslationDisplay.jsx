import React from 'react';

const TranslationDisplay = ({ translation }) => {

  return (
    <div>
      {
        translation.map((image, index) => (
          <img className="hand" key={index} src={`../individial_signs/${image}.png`} alt="sign-hand-pic" />
        ))
      }
    </div>
  )
}

export default TranslationDisplay;