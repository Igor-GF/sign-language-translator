import React from 'react';
import { useState } from 'react'
import { useNavigate } from 'react-router-dom';
import TranslationDisplay from './TranslationDisplay';
import { updateTranslation } from './TranslatorApi';
import { TranslationContext } from '../../Contexts/TranslationContext';
import { useContext } from 'react';
import { UserContext } from '../../Contexts/UserContext';
import { LoginStateContext } from '../../Contexts/LoginStateContext';
import { useEffect } from 'react';

const Translator = () => {

  //-- ContextApi imports
  const { translation, setTranslation } = useContext(TranslationContext)
  const { credentials, setCredentials } = useContext(UserContext)
  const { loginState, setLoginState } = useContext(LoginStateContext)

  //-- Local state
  const [search, setSearch] = useState("");

  //-- Router
  const navigate = useNavigate()

  //-- Side Effects
  useEffect(() => {
    if (loginState === false) {
      navigate('/')
    }
  }, [loginState] )

  //-- Handling the translations
  const onChangeHandler = (e) => {
    
    setSearch(e.target.value);
  
  }

  const convertLanguage = (search) => {
    console.log(search)
    search = search.replace(/[0-9]|[^\w\s]/g,"");
    const splitSearch = search.split(" ").join("").split("");
    setTranslation(splitSearch);
  }

  const onSubmitHandler = (e) => {
    e.preventDefault();
    convertLanguage(search);

    //-- Here also update the database. - search is the text that needs to be updated.
    updateTranslationDatabaseNew(search);
  }

  //-- Function to go to there profilepage
  function goToProfile() {
    navigate('/profile')
  }

  //-- The translations should also go to the database
  function updateTranslationDatabaseNew (search) {
    let updateUser = credentials

    if (updateUser === null) {
      updateUser = credentials[0]
    }

    updateUser.translations.unshift(search)

    let userId = updateUser.id
    let newTranslation = updateUser.translations

    updateTranslation(userId, newTranslation)
  }

  return (
    <div className="container-fluid border border-warning rounded-3 p-4 mt-4">

      <form className="input-group mb-3" onSubmit={onSubmitHandler}>
        <input id="search" type="text" className="form-control" placeholder="Type here to translate..." 
        onChange={onChangeHandler}/>
        <button className="btn btn-outline-secondary" type="submit">translate</button>
      </form>

      <div className="border border-primary rounded-3 p-4  mb-3">
        <TranslationDisplay translation={translation}/>
      </div>

      <div className="d-flex col-6">
        <div className="row">
          <button onClick={goToProfile} className="btn btn-primary col ml-3" > Go to profile </button>
        </div>
      </div>  

    </div>
  )
}

export default Translator;