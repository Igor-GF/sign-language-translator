import React from 'react';
import { useContext } from 'react';
import { UserContext } from '../../Contexts/UserContext';
import { LoginStateContext } from '../../Contexts/LoginStateContext';
import { TranslationContext } from '../../Contexts/TranslationContext';

const Navbar = () => {
  //-- Import the contextAPI
  const { credentials, setCredentials} = useContext(UserContext)
  const { loginState, setLoginState } = useContext(LoginStateContext)
  const { translation, setTranslation } = useContext(TranslationContext)

  //-- Set the credentials to empty
  const emptyCredentials = {
    id: '',
    username: '',
    translations: []
  }

  const emptyTranslations = []
  
  //-- This function is to go back to the homepage is the loginstatus is false
  function goToHomeAndLogout() {
    setCredentials(emptyCredentials)
    setTranslation(emptyTranslations)
    setLoginState(false)
  }

  return (
    <nav className="navbar navbar-light bg-warning">
      <div className="container-fluid row">
        
        <span className="navbar-brand col-4 mb-0 h1 text-primary">Lost in Translation</span>

        { loginState && <span onClick={goToHomeAndLogout} className="btn btn-outline-dark col-4"><strong>Logout: {credentials.username}</strong></span> }
          
      </div>
    </nav>
  )
}

export default Navbar;
