# Assignment 3 - React - Sign Language Translator

[![web](https://img.shields.io/static/v1?logo=heroku&message=Online&label=Heroku&color=430098)](https://fast-inlet-20119.herokuapp.com/)

## Assignment
This application is a Sign Language Translator as a Single Page Application using  React.js framework. This project is part of Noroff Frontend Traneeship. The assignment requires knowlegde of the js framework's fundamentals, specifically React.js, which covers fetching an API, functional components, dealing with states, props, and lifecicle hooks. 

## Use the app click on the link bellow
https://fast-inlet-20119.herokuapp.com/

## Design Component tree
![Component tree](./public/images/component-tree.PNG)

## Contributing
I am open to receive advice and tips which can help me to improve the design, the logic, to clean up the code, etc. So that I can improve myself as a developer.

## Built With
[Microsoft VSCode](https://code.visualstudio.com/)

## Technologies
- ReactJS
- JavaScript
- HTML
- CSS

## Author
[Dianto Bosman](https://gitlab.com/diantobosman1)

[Igor Figueiredo](https://gitlab.com/Igor-GF)

## Acknowledgment
[Dean von Schoultz](https://gitlab.com/deanvons) who is the tutor of the Frontend course at Noroff.

## Project status
Project exclusively made for study purposes.

## License
[MIT](https://choosealicense.com/licenses/mit/)
